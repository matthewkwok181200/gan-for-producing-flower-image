# import image

image_path ="/content/drive/My Drive/Colab Notebooks/flower_photo/daisy/"


import os
# from PIL import Image
import cv2
reshape_size = (64,64)

i = 0
for image in os.listdir(image_path):
  # print(image)
  img = cv2.imread(image_path + image)
  img = cv2.resize(img, reshape_size)
  cv2.imwrite("resized_images/%d.png" % i,img)
  # # print(img.shape)
  i = i+1


from PIL import Image

array = []
path ="/content/resized_images/"
for dir in os.listdir(path):
            # print(dir)
  image = Image.open(path + dir)
  data = np.asarray(image)
  array.append(data)

X_train = np.array(array)



from keras.models import Sequential
from keras.layers import Reshape
from keras.layers import Flatten
from keras.layers import Conv2D, Dense, Conv2DTranspose
from keras.layers import Dropout
from keras.layers import LeakyReLU
from tensorflow.keras.optimizers import Adam
import numpy as np
!mkdir generated_images resized_images

img_width = 64
img_height = 64
channels = 3
img_shape = (img_width, img_height, channels)
latent_dim = 100
adam = Adam(lr=0.0002)



# build generator

def build_generator():
    model = Sequential()
    model.add(Dense(256 * 8* 8, input_dim=latent_dim))
    model.add(LeakyReLU(alpha=0.2))
    model.add(Reshape((8,8,256)))

    model.add(Conv2DTranspose(128, (4,4), strides=(2,2), padding='same'))
    model.add(LeakyReLU(alpha=0.2))

    model.add(Conv2DTranspose(128, (4,4), strides=(2,2), padding='same'))
    model.add(LeakyReLU(alpha=0.2))

    model.add(Conv2DTranspose(128, (4,4), strides=(2,2), padding='same'))
    model.add(LeakyReLU(alpha=0.2))

    model.add(Conv2D(3, (3,3), activation='tanh', padding='same'))
  
    model.summary()

    return model

generator = build_generator()


# build discriminator

def build_discriminator():
    model = Sequential()
    model.add(Conv2D(64, (3,3), padding='same', input_shape=img_shape))
    model.add(LeakyReLU(alpha=0.2))

    model.add(Conv2D(128, (3,3), padding='same', ))
    model.add(LeakyReLU(alpha=0.2))
    
    model.add(Conv2D(128, (3,3), padding='same'))
    model.add(LeakyReLU(alpha=0.2))

    model.add(Conv2D(256, (3,3), padding='same'))
    model.add(LeakyReLU(alpha=0.2))

    model.add(Flatten())
    model.add(Dropout(0.4))
    model.add(Dense(1, activation='sigmoid'))

    model.summary()
    return model

discriminator = build_discriminator()
discriminator.compile(loss='binary_crossentropy', optimizer=adam, metrics=['accuracy'])

# encoder decoder model

GAN = Sequential()
discriminator.trainable = False
GAN.add(generator)
GAN.add(discriminator)

GAN.compile(loss='binary_crossentropy', optimizer=adam)

## training

batch_size = 64
epochs = 1000
valid = np.ones((batch_size, 1))
fakes = np.zeros((batch_size, 1))

for epoch in range(epochs):
   
  idx = np.random.randint(0, X_train.shape[0], batch_size)
  imgs = X_train[idx]

      #Generate Fake Images
  noise = np.random.normal(0, 1, (batch_size, latent_dim))
  gen_imgs = generator.predict(noise)

      #Train discriminator
  d_loss_real = discriminator.train_on_batch(imgs, valid)
  d_loss_fake = discriminator.train_on_batch(gen_imgs, fakes)
  d_loss = 0.5 * np.add(d_loss_real, d_loss_fake)

  noise = np.random.normal(0, 1, (batch_size, latent_dim))
      
      #inverse y label
  g_loss = GAN.train_on_batch(noise, valid)

  print("******* %d [D loss: %f, acc: %.2f%%] [G loss: %f]" % (epochs, d_loss[0], 100* d_loss[1], g_loss))


  #prediction

noise = np.random.normal(0, 1, (16, latent_dim))
gen_imgs = generator.predict(noise)
gen_imgs = (gen_imgs + 1) / 2.0
# plt.imshow(gen_imgs[2])
plt.imshow(gen_imgs[6])
